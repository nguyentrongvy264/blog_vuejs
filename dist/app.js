/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// import module

var _regenerator = __webpack_require__(1);

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(2);

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Hapi = __webpack_require__(3),
    mongoose = __webpack_require__(4),
    config = __webpack_require__(5),
    Boom = __webpack_require__(7),
    Mustache = __webpack_require__(8);

// create server    
var server = Hapi.server({
    port: config.port.www,
    host: "localhost"
    // routes: {
    //   files: {
    //     relativeTo: Path.resolve(__dirname, "./www/")
    //   }
    // }
});

// START server

var init = function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.next = 2;
                        return server.register([{ plugin: __webpack_require__(9) }, { plugin: __webpack_require__(10) }]);

                    case 2:
                        // await server.register(require('./registers'));

                        // console.log(server);
                        server.views({
                            engines: {
                                html: {
                                    compile: function compile(template) {
                                        Mustache.parse(template);
                                        return function (context) {
                                            return Mustache.render(template);
                                        };
                                    }
                                }
                            },
                            //relativeTo: __dirname,
                            path: "./views"
                        });

                        // await server.route(require('./router/auth.js').concat(require('./router/app.js')));

                        _context.next = 5;
                        return server.start();

                    case 5:
                        _context.next = 7;
                        return global.Promise;

                    case 7:
                        mongoose.Promise = _context.sent;
                        _context.next = 10;
                        return mongoose.connect(config.db, {
                            useMongoClient: true,
                            promiseLibrary: global.Promise
                        });

                    case 10:
                        global.db = _context.sent;


                        console.log("Server running at: " + server.info.uri);

                    case 12:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function init() {
        return _ref.apply(this, arguments);
    };
}();

server.route({
    method: "GET",
    //path: '/',
    path: "/{path*}",
    config: {
        handler: function handler(request, h) {
            return h.view("index.html", {});
        }
    }
});

// stop server if error 

process.on("unhandledRejection", function (err) {
    console.log(err);
    process.exit(1);
});

init();

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/regenerator");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("babel-runtime/helpers/asyncToGenerator");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("hapi");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(6);

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Set the 'development' environment configuration object

module.exports = {
    port: {
        www: 300,
        admin: 8081
    },
    db: "mongodb://localhost/blog",
    secret: "78ad3c62b2f44ec3b65f09ebf1236a6c3c56656d6593b39c250646ee375d2075"

};

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("boom");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("mustache");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("vision");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("inert");

/***/ })
/******/ ]);