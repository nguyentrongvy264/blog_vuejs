"use strict";

// import module
var Hapi = require("hapi"),
    mongoose = require('mongoose'),
    config = require('./config'),
    Boom = require("boom"),
    Mustache = require('mustache');


// create server    
const server = Hapi.server({
    port: config.port.www,
    host: "localhost"
    // routes: {
    //   files: {
    //     relativeTo: Path.resolve(__dirname, "./www/")
    //   }
    // }
});




// START server

const init = async () => {
 
    await server.register([
                {plugin: require("vision")},
                {plugin: require("inert")}
        ]);
    // await server.register(require('./registers'));

        // console.log(server);
    server.views({
        engines: {
          html: {
            compile: function(template) {
              Mustache.parse(template);
              return function(context) {
                return Mustache.render(template, 
                
                
                );
              };
            }
          }
        },
        //relativeTo: __dirname,
        path: "./views"
      });

    // await server.route(require('./router/auth.js').concat(require('./router/app.js')));

    await server.start();

    mongoose.Promise = await global.Promise;

    global.db = await mongoose.connect(config.db, {
        useMongoClient: true,
        promiseLibrary: global.Promise
    });

    console.log(`Server running at: ${server.info.uri}`);


}

server.route(
    {
        method: "GET",
        //path: '/',
        path: "/{path*}",
        config: {
          handler: (request, h) => {
            return h.view("index.html", {});
          }
        }
      }
)

// stop server if error 

process.on("unhandledRejection", err => {
    console.log(err);
    process.exit(1);
});

init();


