"use strict";

// Set the 'development' environment configuration object
module.exports = {
    port: {
        www: 300,
        admin: 8081
    },
    db: "mongodb://localhost/blog",
    secret: "78ad3c62b2f44ec3b65f09ebf1236a6c3c56656d6593b39c250646ee375d2075",

}   