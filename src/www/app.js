import Css from "./assets/css/layout.css";

// import "vue-multiselect/dist/vue-multiselect.min.css";
import Vue from "vue";
import App from "./app.vue";
import store from "./store";
import router from "./router";
import { sync } from "vuex-router-sync";
// import Cookie from "vue-cookie";
// import Validate from "vee-validate";
// import NProgress from "vue-nprogress";
import Bootstrap from "bootstrap";
// import Multiselect from "vue-multiselect";
// import MixTable from "v-mix-table";

// import Editor from "v-markdown-editor";
// 

console.log(router);


// global register

// Vue.use(Editor);

sync(store, router);

// Vue.use(MixTable);

// Vue.use(Multiselect);

// Vue.use(Cookie);

// Vue.use(Validate);

// const options = {
//   latencyThreshold: 200, // Number of ms before progressbar starts showing, default: 100,
//   router: true, // Show progressbar when navigating routes, default: true
//   http: false // Show progressbar when doing Vue.http, default: true
// };

// Vue.use(NProgress, options);


// const nprogress = new NProgress();

Vue.config.devtools = true;

const app = new Vue({
    el: "#app",
    //   nprogress,
    router,
    store,
    render: h => h(App)
});

export { app, router, store };