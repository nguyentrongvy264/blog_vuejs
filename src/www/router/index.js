import Vue from "vue";
import Router from "vue-router";
import Home from '../pages/Home.vue';

Vue.use(Router);

// route-level code splitting
// const Home = () => import("../pages/Home.vue");

const router = new Router({
    mode: "history",
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: "/",
            component: Home,
            name: "page-home",
            // meta: {
            //     showProgressBar: false
            // }
        }
    ]
});

module.exports = router;