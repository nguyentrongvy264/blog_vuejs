var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin');
var nodeExternals = require('webpack-node-externals');


module.exports = function (env) {
    return [
        {
            watch: true,
            target: 'node',
            entry: {
                'app': './src/server/app.js',
                // 'api': './src/server/api.js',
                // 'admin': './src/server/admin.js',
                // 'job': './src/server/job.js'

            },
            output: {
                path: path.join(__dirname, './dist'),
                filename: '[name].js',

            },
            module: {
                rules: [{
                    test: /\.js$/,
                    loader: 'babel-loader',
                    //exclude: /node_modules/
                },
                {
                    test: /\.html$/,
                    loader: 'html-loader',
                    query: {
                        minimize: false
                    }
                },
                {
                    test: /\.tpl$/,
                    loader: 'html-loader',
                    query: {
                        minimize: false
                    }
                }

                ]
            },
            //externals: [/^(?!\.|\/).+/i,],
            externals: [nodeExternals()],
            plugins: [
                new webpack.DefinePlugin({
                    'process.env': { NODE_ENV: JSON.stringify(env) }
                })
            ]
        },

        // client 

        {

            entry: {
                'www': './src/www/app.js',
                // 'admin': './src/admin/app.js',

            },
            output: {
                path: path.join(__dirname, './dist'),
                //filename: '[name].[hash:8].js',
                filename: '[name]/js/app.js',
                publicPath: '/',
                //chunkFilename: '[name]-chunk.js',
                //sourceMapFilename: '[name].map'
            },
            module: {
                rules: [{
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            // presets: ['es2015', "stage-2"],
                            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                            // the "scss" and "sass" values for the lang attribute to the right configs here.
                            // other preprocessors should work out of the box, no loader config like this nessessary.
                            'scss': 'vue-style-loader!css-loader!sass-loader',
                            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                        },
                        // other vue-loader options go here
                    }
                },
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: 'babel-loader',
                },

                {
                    test: /\.json$/,
                    loader: 'json-loader'

                },
                {
                    test: /\.css$/,
                    use: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: "css-loader",

                    })
                    //use: ['style-loader', 'css-loader'],
                    // options: {
                    //     minimize: true
                    // },
                },
                {
                    test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: "url-loader?limit=100000"
                }
                ]
            },
            // externals: [/^(?!\.|\/).+/i,],
            //externals:/^(jquery|\$|bootstrap|vue)$/i,
            devtool: '#source-map',
            plugins: [
                new webpack.DefinePlugin({
                    'process.env': { NODE_ENV: JSON.stringify(env) }
                }),

                new HtmlWebpackExternalsPlugin({
                    externals: [
                        {
                            module: 'jquery',
                            entry: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                            global: 'jQuery'
                        },
                        {
                            module: 'Popper',
                            entry: 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.1/umd/popper.min.js'
                        },
                        {
                            module: 'boostrap',
                            entry: 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js'
                        },
                        {
                            module: 'vue',
                            global: 'Vue',
                            entry: 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js'
                        },
                        {
                            module: 'vue-router',
                            global: 'VueRouter',
                            entry: 'https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.min.js'
                        },

                        {
                            module: 'vuex',
                            global: 'Vuex',
                            entry: 'https://cdnjs.cloudflare.com/ajax/libs/vuex/3.0.1/vuex.min.js'
                        }

                    ]
                }),


                new HtmlWebpackPlugin({
                    filename: 'views/index.html',
                    template: 'src/server/views/index.html',
                    hash: true,
                    chunks: ['www'],
                    minify: {
                        collapseWhitespace: true,
                        //preserveLineBreaks: true,                    
                        removeComments: true,
                        collapseBooleanAttributes: true,
                        removeEmptyAttributes: true
                    }

                }),
             


                new ExtractTextPlugin({
                    filename: '[name]/css/app.css',
                    // filename: (getPath) => {
                    //     return getPath('[name].css');//.replace('js', 'css');
                    //     //return getPath('[name].[hash:8].css').replace('js', 'css');
                    // },
                    allChunks: true

                }),


                new CopyWebpackPlugin([
                    // { from: 'src/www/favicon/**/*', to:'/www/favicon/' },
                    {
                        context: 'src/www/favicon',
                        from: '**/*',
                        to: './www/favicon'
                    }
                ])
            ]
        }

    ]
}